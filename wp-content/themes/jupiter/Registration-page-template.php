
<?php
/*
Template Name: userregistration
*/


get_header(); ?>
<div id="theme-page">
	<div class="mk-main-wrapper-holder">
	<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper full-layout vc_row-fluid mk-grid row-fluid">
		<div class="theme-content" itemprop="mainContentOfPage">
			
		
				<div style="padding: 20px 0 40px;" class="mk-divider mk-shortcode divider_full_width double_dot "><div class="divider-inner"></div></div>
				
				<?php
				echo do_shortcode('[calendarizeit]');
				?>
				
				<div style="padding: 20px 0 40px;" class="mk-divider mk-shortcode divider_full_width double_dot "><div class="divider-inner"></div></div>
				<?php  echo do_shortcode('[cr_custom_registration]');?>
			

				
		</div>
	<div class="clearboth"></div>	
	</div>
	<div class="clearboth"></div>
	</div>
</div>
<?php get_footer(); ?>