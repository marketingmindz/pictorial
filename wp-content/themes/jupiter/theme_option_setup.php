<?php
/**
 * Implement an optional theme options for Gift Portal
 *
 * See http://codex.wordpress.org/Gift_Portal
 *
 * @package WordPress
 * @subpackage Gift_Portal
 * @since Gift Portal 1.0
 */

/**
 * Set up the WordPress core theme options arguments and settings.
 *
 * @uses add_theme_support() to register support for 3.4 and up.
 * @uses giftportalwebsite_header_style() to style front-end.
 * @uses giftportalwebsite_admin_header_style() to style wp-admin form.
 * @uses giftportalwebsite_admin_header_image() to add custom markup to wp-admin form.
 *
 * @since Gift Portal 1.0
 */
 

$active = isset($_REQUEST['paypal'])?$_REQUEST['paypal']:'main';
$message = isset($_REQUEST['message'])?$_REQUEST['message']:'';
$error_msg = "";


if(isset($_REQUEST['paypal_submit']) && isset($_REQUEST['_wpnonce']) && !empty($_REQUEST['_wpnonce'])){
	
	update_option( 'paypal_business', $_REQUEST['paypal_business'] );
	
	update_option( 'paypal_type', $_REQUEST['account_type'] );
	
	update_option( 'paypal_redirect_url', $_REQUEST['redirect_url'] );
	
	$message = "Successfully%20updated.";

	wp_redirect("admin.php?page=paypal_options&message=$message");
	exit;
	
}

?>

<div class="wrap">
    
	<h2 class="nav-tab-wrapper">
        <a href="admin.php?page=paypal_options" class="nav-tab nav-tab-active">Paypal</a>
    </h2>
    
    <?php if( !empty ( $message ) ): ?>
    <div id="message" class="updated below-h2">
    	<p><?php echo __( $message, 'paypalwebsite'); ?></p>
    </div>
    <?php endif; ?>
    
    <?php
    
		
		$paypal_business	  = get_option( 'paypal_business', true );
		$account_type	  	 = get_option( 'paypal_type', true );
		$redirect_url	  	 = get_option( 'paypal_redirect_url', true );
		?>
        <div id="menu-locations-wrap">
            <form method="post" action="">
                <table class="form-table">
                    <tbody>
                        <tr>
                            <td><strong>Paypal Business email</strong></td>
                            <td><input type="text" name="paypal_business" value="<?php echo $paypal_business; ?>" size="30" /></td>
                        </tr>
                        
                        <tr>
                            <td><strong>Account Type</strong></td>
                            <td>
                            	Sandbox <input type="radio" name="account_type" value="sandbox" <?php if($account_type == 'sandbox' ){ ?>checked="checked"<?php } ?> /> 
                                Main <input type="radio" name="account_type" value="main" <?php if($account_type == 'main' ){ ?>checked="checked"<?php } ?> />
                            </td>
                        </tr>
                        
                        <tr>
                            <td><strong>Redirect URL</strong></td>
                            <td><input type="text" name="redirect_url" value="<?php echo $redirect_url; ?>" size="30" /></td>
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>
                        
                    </tbody>
                </table>
                <p class="button-controls"><input type="submit" name="paypal_submit" id="paypal_submit" class="button button-primary left" value="Save Changes"></p>
                <input type="hidden" id="_wpnonce" name="_wpnonce" value="344911a67c">
            </form>
        </div><!-- #menu-locations-wrap -->
   
    
</div>