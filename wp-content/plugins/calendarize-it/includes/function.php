<?php
add_action( 'add_meta_boxes', 'add_events_metaboxes' );
// Add the Events Meta Boxes


// Add the Events Meta Boxes

function add_events_metaboxes() {
	
	add_meta_box('wpt_events_location', 'Event Type', 'wpt_events_location', RHC_EVENTS, 'normal', 'high');
}
// The Event Location Metabox

function wpt_events_location() {
	global $post;
	
	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	
	// Get the location data if its already been entered
	$event_avialbale_status = get_post_meta($post->ID, 'event_avialbale_type', true);
	
	// Echo out the field
	
 if($event_avialbale_status==1)
	{
	$optin2="checked='checked'";
	}
	
	echo 'Group <input type="checkbox" '.$optin2.' name="event_avialbale_type" value="1" class="widefat"> ';

}
// Save the Metabox Data

function wpt_save_events_meta($post_id, $post) {
	
	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.
	
	$events_meta['event_avialbale_type'] = $_POST['event_avialbale_type'];
	
	// Add values of $events_meta as custom fields
	
	foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
	}

}

add_action('save_post', 'wpt_save_events_meta', 1, 2); // save the custom fields
// The Event Location Metabox
//Register Custom Function
add_action( 'init', 'create_my_taxonomies', 0 );
//Implementation of Custom Function and Registering Custom Taxonomy
function create_my_taxonomies() {
    register_taxonomy(
        'private_venue_genre',
        'events',
        array(
            'labels' => array(
                'name' => 'Private Venue',
                'add_new_item' => 'Add New private Venue',
                'new_item_name' => "New Venue private Genre"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
}

// Add term page
function private_venue_genre_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	$args = array(
    'orderby'           => 'name', 
    'order'             => 'ASC',
    'hide_empty'        => true, 
    'exclude'           => array(), 
    'exclude_tree'      => array(), 
    'include'           => array(),
    'number'            => '', 
    'fields'            => 'all', 
    'slug'              => '', 
    'parent'            => '',
    'hierarchical'      => true, 
    'child_of'          => 0, 
    'get'               => '', 
    'name__like'        => '',
    'description__like' => '',
    'pad_counts'        => false, 
    'offset'            => '', 
    'search'            => '', 
    'cache_domain'      => 'core'
); 

global $wpdb;
$levels = get_terms( 'level', 'hide_empty=0' );

	?>
	<div class="form-field">
		<label for="level"><?php _e( 'Level', 'private_venue_genre' ); ?></label>
		<select name="term_meta[]"  id="venue_level" multiple> <option value="0"> Select a level </option>
 <?php 
 for($i=0;count($levels) >$i; $i++)
 {
  ?>
  <option value="<?php echo $levels[$i]->term_id;?>"> <?php echo $levels[$i]->name;?> </option>
  <?php
 }
 ?>
		</select>
		<p class="description"><?php _e( 'select level of private session','private_venue_genre' ); ?></p>
	</div>
	
<?php
}
add_action( 'private_venue_genre_add_form_fields', 'private_venue_genre_taxonomy_add_new_meta_field', 10, 2 );


// Edit term page
function private_venue_genre_taxonomy_edit_meta_field($term) {
	// put the term ID into a variable
$t_id = $term->term_id;
$levels = get_terms( 'level', 'hide_empty=0' );

	// retrieve the existing value(s) for this meta field. This returns an array
	
$term_meta=explode(",",get_term_meta($t_id,'venue_level',true));

	?>
	<style>
/* Below line is used for online Google font */
@import url(http://fonts.googleapis.com/css?family=Droid+Serif);
select{
width::80%;
border:3px solid rgb(200, 200, 207);
height:42px;
padding:5px;
border-radius:3px;
margin:5px 10px 20px 35px;
}
input[type=text]{
width:76%;
border:3px solid rgb(200, 200, 207);
height:23px;
padding:5px;
border-radius:3px;
margin:5px 10px 20px 35px;
}
input[type=submit]{
width:80%;
border:3px solid rgb(200, 200, 207);
height:42px;
padding:5px;
border-radius:3px;
margin:10px 10px 20px 35px;
border:1px solid green;
}
label{
margin-left:35px;
font-family: 'Droid Serif', serif;
}
.container{
width:960px;
margin:50px auto;
}
.main{
float:left;
width:355px;
height:350px;
box-shadow:1px 1px 12px gray;
padding-top:30px;
}
h2{
width:370px;
text-align:center;
font-family: 'Droid Serif', serif;
}
	</style>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="venue_level"><?php _e( 'Level', 'pippin' ); ?></label></th>
		<td>
			<select name="term_meta[]"  id="venue_level" multiple> <option value="0"> Select a level </option>
 <?php 
 for($i=0;count($levels) >$i; $i++)
 {
  ?>
  <option <?php if(!empty($term_meta)) { if(in_array($levels[$i]->term_id,($term_meta))) { echo 'selected="selected"';} }?> value="<?php echo $levels[$i]->term_id;?>"> <?php echo $levels[$i]->name;?> </option>
  <?php
 }
 ?>
		</select>
			<p class="description"><?php _e( 'Enter a value for this field','pippin' ); ?></p>
		</td>
	</tr>
<?php
}
add_action( 'private_venue_genre_edit_form_fields', 'private_venue_genre_taxonomy_edit_meta_field', 10, 2 );
// Save extra taxonomy fields callback function.
function save_taxonomy_custom_private_meta( $term_id ) {

	if ( isset( $_POST['term_meta'] ) ) {
		$levelID=$_POST['term_meta'];
		$return="";
for($i=0;count($levelID)>$i;$i++)
{
$return.=$levelID[$i] .",";
}

		// Save the option array.
		update_term_meta($term_id, 'venue_level',$return);
		
	}
	
} 
add_action( 'edited_private_venue_genre', 'save_taxonomy_custom_private_meta', 10, 2 );  
add_action( 'create_private_venue_genre', 'save_taxonomy_custom_private_meta', 10, 2 );
?>