<?php
ob_start();
if(!session_id()) {
        session_start();
}
/**
* Plugin Name: Event Registration
* Description: Event Registration .
* Version: 1.0
* Author: info

**/
define( 'REGISTRATION_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'REGISTRATION_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
/**********************
Info activation of the plugin
Developer : chandrabhan
date 10/3/2014

***********************/

function registration_plugin_activate() {
	add_option( 'Activated_Plugin', 'registration_plugin' );
	/* Fetch branches and property here  */
}
register_activation_hook( __FILE__, 'registration_plugin_activate' );
/**********************
Load region Plugin 

Developer : chandrabhan
date 10/3/2014
***********************/
function load_plugin() {

	if ( is_admin() && get_option( 'Activated_Plugin' ) == 'registration_plugin' ) {
		global $wpdb;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

global $wpdb;
			/* Create table at_loop for Loop insertion if not exist */
			$table_name = 'chart_date';
			if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {  // If start for couponfeed table creation
			$chart_date  ="CREATE TABLE IF NOT EXISTS `wp_check_reg` (".
							"`id` int(11) NOT NULL,".
							"`user_id` int(11) NOT NULL,".
							"`session` int(11) NOT NULL,".
							"`level` int(11) NOT NULL,".
							"`schedule` int(11) NOT NULL,".
							"`status` enum('No','Yes') NOT NULL".
							")";
			dbDelta($chart_date);
			}
    }
}
add_action( 'admin_init', 'load_plugin' );
add_action( 'admin_init', 'registration_custom_menu_page' );


/*custom menu page*/
function registration_custom_menu_page(){

$page = get_page_by_title('Registration');
  if(empty($page))
	{
		// Create page object
		$my_post = array(
		'post_title'    => 'Registration',
		'post_status'   => 'publish',
		'post_author'   => 1,
		'post_type'     => 'page',
		'page_template' =>'Registration-page-template.php'
		);

		// Insert the post into the database
    wp_insert_post( $my_post );
	}

 
	}
require_once(REGISTRATION_PLUGIN_DIR."registerForm.php");
add_action('admin_init', 'my_general_section');  
function my_general_section() {  
    add_settings_section(  
        'my_settings_section', // Section ID 
        'User Registration dropdown', // Section Title
        'my_section_options_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

   

    add_settings_field( // Option 2
        'Your_Camera_Brand', // Option ID
        'Your Camera Brand', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'Your_Camera_Brand' // Should match Option ID
        )  
    ); 
add_settings_field( // Option 1
        'pictorial_referance *', // Option ID
        'How Did You Get to Know Pictorial Website?: ', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'pictorial_referance' // Should match Option ID
        )  
    ); 
   
    register_setting('general','Your_Camera_Brand', 'esc_attr');
	register_setting('general','pictorial_referance', 'esc_attr');
}

function my_section_options_callback() { // Section Callback
    echo '<p>Please Enter  comma-separated values </p>';  
}

function my_textbox_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
   echo '<textarea id="'. $args[0] .'" name="'. $args[0] .'" cols="100" rows="5">'.$option.'</textarea>';
}
function registration_plugin_deactivate() {
	$url = admin_url();
	wp_redirect($url);
	exit;
	/* Fetch brenches and property here  */
}

//Om Coding

// hook into the init action and call create_level_taxonimy when it fires
add_action( 'init', 'callback_init');
function callback_init(){
		
		if(is_plugin_active('calendarize-it/calendarize-it.php')){
			// Add new taxonomy, make it hierarchical (like categories)
			$labels = array(
				'name'              => 'Levels',
				'singular_name'     => 'Level',
				'search_items'      => __( 'Search Levels' ),
				'all_items'         => __( 'All Levels' ),
				'parent_item'       => __( 'Parent Level' ),
				'parent_item_colon' => __( 'Parent Level:' ),
				'edit_item'         => __( 'Edit Level' ),
				'update_item'       => __( 'Update Level' ),
				'add_new_item'      => __( 'Add New Level' ),
				'new_item_name'     => __( 'New Level Name' ),
				'menu_name'         => __( 'Levels' ),
			);
		
			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'level' ),
			);
		
			register_taxonomy( 'level', array( 'events' ), $args );
			
			
			
			
			
			
			add_action( 'level_add_form_fields', 'registration_taxonomy_add_new_meta_field', 10, 2 );
			add_action( 'level_edit_form_fields', 'registration_taxonomy_edit_meta_field', 10, 2 );
			add_action( 'edited_level', 'save_taxonomy_custom_meta', 10, 2 );  
			add_action( 'create_level', 'save_taxonomy_custom_meta', 10, 2 );
			
			add_filter("manage_edit-level_columns", 'level_taxonomy_column'); 
			add_filter("manage_level_custom_column", 'manage_theme_columns', 10, 3);
		}
}





// Add term page
function registration_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[custom_term_meta]"><?php _e( 'Number of Session', 'registration' ); ?></label>
		<input type="text" name="." id="term_meta[custom_term_meta]" value="">
		<p class="description"><?php _e( 'Enter a value for this field','registration' ); ?></p>
	</div>
<?php

}

// Edit term page
function registration_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Number of Session', 'registration' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="<?php echo esc_attr( $term_meta['custom_term_meta'] ) ? esc_attr( $term_meta['custom_term_meta'] ) : ''; ?>">
			<p class="description"><?php _e( 'Enter a value for this field','registration' ); ?></p>
		</td>
	</tr>
<?php
}

// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}


function level_taxonomy_column($columns) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'name' => __('Name'),
		'description' => __('Description'),
        'slug' => __('Slug'),
		'session' => __('No. of Session'),
        'posts' => __('Count'),
		'price' => __('Price'),
		'extra_information' => __('Extra Information')
        );
    return $new_columns;
}

function manage_theme_columns($out, $column_name, $t_id) {
	
    $term_meta = get_option( "taxonomy_$t_id" );
	//print_r($term_meta);
	//print_r($term_meta['custom_term_meta']);
	
    switch ($column_name) {
        case 'session': 
            $out .= $term_meta['custom_term_meta']; 
            break;
		case 'price':
			echo get_field("price","level_".$t_id);
			 break;
		case 'extra_information':
			echo get_field("extra_information","level_".$t_id);
			//echo $t_id;
			break;
			
        default:
            break;
    }
    return $out;    
}
function theme_name_scripts() {
	 
	wp_enqueue_script( 'script-name', REGISTRATION_PLUGIN_URL . 'js/register.js', array(), '1.0.0', true );
	wp_enqueue_script( 'timepikcer', REGISTRATION_PLUGIN_URL . 'js/jquery.timepicker.min.js', array(), '1.0.0', true );
	wp_enqueue_style( 'register', REGISTRATION_PLUGIN_URL . 'css/register.css');
	wp_enqueue_style( 'tiem', REGISTRATION_PLUGIN_URL . 'css/jquery.timepicker.css');
	wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	
}


add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
?>